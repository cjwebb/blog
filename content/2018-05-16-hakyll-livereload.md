+++
title = "Hakyll Live Reload"
date = 2018-05-16

[taxonomies]
tags = ["haskell"]
+++

I use [LiveReloadX](http://nitoyon.github.io/livereloadx/) to automatically reload the browser as I work on my Hakyll-generated blog. LiveReloadX, and most other automatic reloading utilities work by watching the file system and notifying something in the browser to refresh the page.

<!-- more -->

When working with Hakyll, I run LiveReloadX as a static web server alongside the normal `stack exec site watch`. The LiveReloadX static web server injects JavaScript into my site to listen for these notifications. By injecting the JavaScript with the LiveReloadX web server, the generated HTML from Hakyll remains unchanged. There is no live-reload scripting included when publishing.

## Usage

```
npm install -g livereloadx
stack exec site watch
livereloadx -s _site
```

A live-reloading version of your Hakyll site will now be available on `http://localhost:35729`.

## Via Docker

If you don't want to install npm or pollute your global node modules, you can run the same thing with a Docker container:

```
docker run --rm -it \
  -p 35729:35729 \
  -v "$(pwd)/_site:/site" \
  node:alpine /bin/ash -c \
  "npm install -g --log-level=error livereloadx; \
  livereloadx -s /site"
```

This is the mechanism I use, and it works wonderfully.
