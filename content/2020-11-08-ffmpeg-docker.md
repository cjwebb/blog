+++
title = "FFmpeg via Docker"
date = 2020-11-08

[taxonomies]
tags = ["python", "ffmpeg", "docker"]
+++

I have [previously written](/posts/python-docker-ffmpeg-h264-mp4) about using [FFmpeg](https://www.ffmpeg.org/about.html) to convert H264 into MP4, running in a Docker container.

Running desktop applications in Docker negates the need to install anything, and isolates processes more effectively than something like Brew would. I needed to use FFmpeg again this week, and so spent a little bit of time setting it up properly - using Docker. Jessie Frazelle wrote a post in 2015 named [Containers on the Desktop](https://blog.jessfraz.com/post/docker-containers-on-the-desktop/) that was the inspiration for doing this.

<!-- more -->

I hadn't realised until writing this post that I'd waited five years to try out something similar to the Containers on the Desktop post, but better late than never! Perhaps I'll get around to learning [Nix](https://nixos.org/) someday too.

Here's the function is added to my shell to use ffmpeg. Usage from the terminal is simply as described by FFmpeg tutorials.

``` bash 
ffmpeg() {
  # https://hub.docker.com/r/jrottenberg/ffmpeg/
  docker run -it --rm \
    -v $(pwd):$(pwd) -w $(pwd) \
    --name ffmpeg \
    jrottenberg/ffmpeg:snapshot-alpine \
    $@
}
```

