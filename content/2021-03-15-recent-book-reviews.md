+++
title = "Recent Books I Have Read"
date = 2021-03-15

[taxonomies]
tags = ["books"]
+++

Quick reviews of three books I've recently read.

1. [The Practice, by Seth Godin](https://www.goodreads.com/book/show/53479927-the-practice)

1. [Zero to One, by Peter Thiel and Blake Masters](https://www.goodreads.com/book/show/18050143-zero-to-one)

1. [The $100 Startup, by Chris Guillebeau](https://www.goodreads.com/book/show/12605157-the-100-startup)

<!-- more -->

These three books are centered around starting a venture; whether a new business, a VC-funded startup, or perhaps a traditional creative endeavour such as writing. I took something from all three.

## The Practice: Shipping Creative Work, by Seth Godin.

This book had two themes.

Firstly, creative work only happens when you sit down and work. Stop waiting for inspiration, and show up. Only by starting, will things happen.

Secondly, don't completely tie the process of your work to the results it receives. Yes, you should listen to feedback and seek to alter the work to fit an audience, but keep in mind that good decisions are still good decisions even if the outcome isn't what you wanted. 

Futhermore, a practice can be its own reward. If you enjoy writing, then write. If you enjoy photography, then take pictures. Engage in it for yourself first.

## Zero to One: Notes on Startups, or How to Build the Future, by Peter Thiel and Blake Masters

Build ambitious things, and sell them.

This book could have been a lot shorter, as it contains a lot of (admittedly entertaining) examples of that statement. It does also have [a checklist of seven questions][1] to ask before committing to a venture that will help me engage in critical thinking.

## The $100 Startup: Reinvent the Way You Make a Living, Do What You Love, and Create a New Future, by Chris Guillebeau.

This was inspirational reading. You can get started without much money, just focus on helping people and building relationships.

A lovely look at small businesses setup by normal people that took a few steps to make their hobbies generate a comfortable income.

The book starts off by exclaiming how simple and easy it is, but by the end, it sounds like a lot of work to run a small business - which is what you'd expect. 

It does successfully highlight that not every business needs to become massive. There are a few examples of people that sought to decrease their company size, and found happiness in doing so.

___

## Wrap Up
I decided to read those books after conversations with old friends about starting small businesses to have as a side income.

Zero to One and the $100 Startup differed in the extreme between their approach. One was unicorns startups with VC funding, and the other was about scaling hobbies a little. Both had strategies, and questions to ask, and I'll be applying those to any venture that I do embark on in the future.

The Practice concerned motivation, and finding pleasure in doing - rather than the results. It may prove to have a greater impact on me than the others.

I am, at least, going to write more...

[1]: https://blog.hypeinnovation.com/peter-thiels-7-questions-for-product-innovation