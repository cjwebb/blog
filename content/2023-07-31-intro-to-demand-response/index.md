+++
title = "An Introduction to Demand Response in Electrical Energy Markets"
date = 2023-07-31

[taxonomies]
tags = ["energy", "renewables"]

[extra]
ogimage = "karsten-wurth-0w-uTa0Xz7w-unsplash.jpg"
+++  

![Photo by @karsten_wuerth on Unsplash](karsten-wurth-0w-uTa0Xz7w-unsplash.jpg)

Demand Response is a mechanism to help balance an electricity grid. Typically, it involves incentives (economic or otherwise) for consumers to move their demand to when supply is plentiful. For example, to move electricity demand from peak times to off-peak times.

<!-- more -->

# What is grid balancing?

Balancing a grid means matching supply to demand. This is tricky as there are peaks and troughs in supply and demand. Demand for electricity is generally higher during the day and lower at night - as people are awake, and then asleep.

Weather plays a big part too. If it is windy there may be a surge in wind generation. Similarly, if its sunny there may be a surge in solar generation. However, both these things can also impact heating and air-conditioning usage that increases demand.

Storing electricity is currently difficult and expensive so the majority of electricity generated must be used immediately. This means that supply and demand must be matched as closely as possible. If a mismatch happens, the grid may become unstable - leading to fluctuations in voltage and AC frequency - and ultimately may result in blackouts and brownouts, and damage to electrical equipment.

# Increase Supply or Reduce Demand

To balance a grid, there are two options: increase supply or decrease demand.

Increasing supply means starting up more power plants, or accurately modelling the output of renewables to account for sunnier or windier weather.

Most power plants are typically slow to start-up - one type that isn't is known as a [Peaking Power Plant](https://en.wikipedia.org/wiki/Peaking_power_plant). However, these typically burn fossil fuels such as natural gas, and are therefore carbon-expensive.

Decreasing demand is where Demand Response comes in. It requires coordination. Utility companies must predict supply and demand, and then signal to consumers to reduce demand. Reducing demand is obviously carbon-cheap, since less energy is consumed.


# Demand Response Examples

There are lots of new and emerging examples of Demand Response.

Charing EVs at night is well known, since there is usually an excess of supply at night. Many energy companies have night-time use tariffs far lower than daytime tariffs to encourage overnight charging.

Another type of EV demand-response is using the car's batteries to discharge into the grid when there is excess demand.

Similarly, home solar can charge battery storage when there is excess supply, and can discharge into the grid when there is excess demand.

Many organisations are now also starting to consider domestic appliances, such as smart thermostats and smart appliciances as demand-response devices. For example, a smart thermostat can be used to imperceptibly reduce heating/cooling at peak times. Or, a washing machine could be delayed until off-peak times - or at least until the sun is shining and the drying function won't necessarily be used.
