# Written with Python 3.8

class Node:
    def __init__(self, val, connected=[]):
        self.val = val
        self.connected = connected


tree = Node("Origin", connected = [
    Node("A", connected=[
        Node("D")
    ]),
    Node("B", connected=[
        Node("E"),
        Node("F")
    ]),
    Node("C")
])


def bfs(tree):
    visited = []
    queue = [tree]
    
    while queue:
        current = queue.pop(0)
        visited.append(current.val)
        for c in current.connected:
            queue.append(c)

    return visited


def dfs(tree):
    visited = []
    stack = [tree]

    while stack:
        current = stack.pop()
        visited.append(current.val)
        for c in current.connected:
            stack.append(c)

    return visited


def dfs_recursive(tree, visited = []):
    visited.append(tree.val)
    for c in tree.connected:
        dfs_recursive(c, visited)
    return visited


if __name__ == '__main__':
    print(f"BFS visit order: {bfs(tree)}")
    print(f"DFS visit order: {dfs(tree)}")
    print(f"DFS recursive  : {dfs_recursive(tree)}")
