import argparse
import csv
from typing import Dict

from mimesis import Address, Person
from mimesis.locales import Locale


def fake_record(person: Person, address: Address) -> Dict:
    return {
        "name": person.full_name(),
        "address": f"{address.address()}, {address.city()}, {address.postal_code()}",
    }


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="Generate Data")
    parser.add_argument("-f", "--file", default="data.csv")
    parser.add_argument("-n", "--num_rows", type=int)
    args = parser.parse_args()

    addr = Address(locale=Locale.EN_GB)
    per = Person(locale=Locale.EN_GB)

    records = (fake_record(per, addr) for _ in range(args.num_rows))

    with open(args.file, "w", newline="") as csvfile:
        writer = csv.DictWriter(
            csvfile, quoting=csv.QUOTE_ALL, fieldnames=["name", "address"]
        )
        for row in records:
            writer.writerow(row)
