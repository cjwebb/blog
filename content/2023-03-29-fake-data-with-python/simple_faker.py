import argparse
import csv
from typing import Dict
from faker import Faker


def fake_record(fake: Faker) -> Dict:
    return {
        "name": fake.name(),
        "address": fake.address(),
    }


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="Generate Data")
    parser.add_argument("-f", "--file", default="data.csv")
    parser.add_argument("-n", "--num_rows", type=int)
    args = parser.parse_args()

    fake = Faker()
    records = (fake_record(fake) for _ in range(args.num_rows))

    with open(args.file, "w", newline="") as csvfile:
        writer = csv.DictWriter(
            csvfile, quoting=csv.QUOTE_ALL, fieldnames=["name", "address"]
        )
        for row in records:
            writer.writerow(row)
