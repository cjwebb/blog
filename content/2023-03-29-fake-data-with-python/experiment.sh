#!/bin/bash

set -euo pipefail

processes=(1) # 2 4 8)
nums=(100000 1000000 10000000 100000000 1000000000)
for n in "${nums[@]}"
do
    for p in "${processes[@]}"
    do
        echo "$n,$p,$(/usr/bin/time python mp_partitions.py -n $n -p 8 2>&1 | awk '{print $1}')"
    done
done
