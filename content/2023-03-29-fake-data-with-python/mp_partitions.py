import argparse
import csv
import multiprocessing as mp
from typing import Dict

from mimesis import Address, Person
from mimesis.locales import Locale


def fake_record(person: Person, address: Address) -> Dict:
    return {
        "name": person.full_name(),
        "address": f"{address.address()}, {address.city()}, {address.postal_code()}",
    }


def worker(file_prefix: str, part: int, n: int):
    addr = Address(locale=Locale.EN_GB)
    per = Person(locale=Locale.EN_GB)

    records = (fake_record(per, addr) for _ in range(n))

    filename = f"{file_prefix}_part_{part}.csv"
    with open(filename, "w", newline="") as csvfile:
        writer = csv.DictWriter(
            csvfile, quoting=csv.QUOTE_ALL, fieldnames=["name", "address"]
        )
        writer.writeheader
        writer.writerows(records)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="Generate Data")
    parser.add_argument("-f", "--file_prefix", default="data")
    parser.add_argument("-n", "--num_rows", type=int, default=1000)
    parser.add_argument("-p", "--num_processes", type=int, default=1)
    args = parser.parse_args()

    rows_per_process = int(args.num_rows / args.num_processes)
    task_args = [
        (args.file_prefix, i, rows_per_process) for i in range(0, args.num_processes)
    ]

    pool = mp.Pool(processes=args.num_processes)
    pool.starmap(worker, task_args)
    pool.close()
