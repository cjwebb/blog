+++
title = "Git Stash: The Basics"
date = 2022-05-20

[taxonomies]
tags = ["git"]
+++

Git stash allows you to quickly push local changes to a stack-based storage area, thereby giving you a clean working state without creating commits or branches.

It is useful when manipulating previous commits, or prototyping with different changesets. Git stash only happens on your local machine and cannot be pushed to a remote server.

<!-- more -->

## Git Stash Usage

If you have working state, `git add` some files and then call `git stash`. Your added changes will be pushed into the stash:


``` sh
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   README.md

$ git add README.md

$ git stash
Saved working directory and index state WIP on master: 16da755 Initial Commit

$ git status
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
```


To retrieve your changes use `git stash apply`.

Git stash is like a stack datastructure. `git stash pop` applies the changes back to your working state **and** removes it from the stack. `apply` does not remove from the stash. Therefore stashed changes will grow over time unless you drop them using `git stash drop`.

`git stash` is an alias for `git stash push`, continuing the stack analogy.


## Git Stash List

To view your git stash, use `git stash list`.

``` bash
$ git stash list
stash@{0}: WIP on master: 16da755 Initial Commit
```

`stash@{0}` is our newly stashed changes. Changes are indexed from zero and the most recent change is given index zero.

By default the message is `WIP on {branch}` followed by a short description of the latest commit. You can change this message when pushing to the stash by using `git stash push -m {message}`. I highly recommend doing this if your stashed changes will last longer than a few hours!


## Git Stash Show

Use `git stash show` to show a summary of the most recent single stashed change.

Since stashed changes are indexed from zero, they can also be shown like this:

```
git stash show stash@{0}
```

You can apply any previous stash using this indexing mechanism too.
