+++
title = "Gravity Batteries At Home?"
date = 2023-06-29

[taxonomies]
tags = ["energy", "renewables", "battery", "physics"]

[extra]
ogimage = "michael-barnes-ztuh3f37d1g-unsplash.jpg"
+++

![](michael-barnes-ztuh3f37d1g-unsplash.jpg)

Gravity batteries are a renewable energy storage mechanism, still being developed for the mainstream. In theory, they are very similar to [pumped hydroelectric](https://en.wikipedia.org/wiki/Pumped-storage_hydroelectricity). In times of low-demand and cheaper electricity, water is pumped up a hill into a reservoir. When electricity demand is high, and price is high, they let the water fall through a generator using the force of gravity.

Gravity batteries are another take on this principle; store energy when it is cheap and plentiful and generate electricity when it is not - through using gravity.

<!-- more -->

In simple terms, a gravity battery lifts an object up and then lets it fall again. The energy used to lift the object is stored as potential energy, and then converted back to electricity when the object falls.

Could I build a gravity battery at home? How much energy could I store? Is it feasible at this scale?


## Mathematics!

The maths here is pretty simple, and governed by just a few equations.

$$U_g = mgh$$

Where `U` is the gravitational potential energy, `m` is the mass of the object we're using, `g` is the gravitational acceleration, and `h` is the height of the object at a specific point in time.

So, the change in energy between two heights can be calculated as:

$$\Delta E = mg (h_2 - h_1)$$

My garage is approximately 4 meters tall, if I raised a mass up into the rafters. I also own a 100kg weight set, so I could use that as my mass to make the calculations easier.


Plugging in the numbers:

$$\Delta E = 100 \times 9.81 \times 4$$
$$\Delta E = 3924.0 \text{ J}$$

Next, we should convert that into something more useful. One Watt is the energy of 1 Joule acting for one second. So, we can calculate the energy in Watt-hours to make it comparable to batteries.

$$\Delta E = 3924.0 \text{ J} \times \frac{1 \text{ W} \cdot \text{s}}{1 \text{ J}} \times \frac{1 \text{ h}}{3600 \text{ s}}$$

$$\Delta E = 1.09 \text{ Wh}$$

My current lithium-ion batteries (connected to solar panels) are ~4kWh.

So, the proposed gravity battery would have 4000 times less energy than my current batteries.

## Gravity Batteries in the wild

This quick bit of maths shows that a gravity battery at home doesn't really compare to lithium-ion.

Scottish start-up Gravitricity are developing gravity batteries in the wild, and planning on using abandoned mine shafts to house the moving weight. These mine shafts are a kilometre long, so ~250 times taller than my garage. They're also using masses of several tens of tonnes. Another order of magnitude larger than my weight set!

This is the scale required to make gravity batteries feasible. The energy density of lithium-ion batteries is just too high to compete with at the small scale.

## More Reading

- Gravitricity has a list of their active projects: [https://gravitricity.com/projects/](https://gravitricity.com/projects/).
- BBC Future have a great article summarising gravity batteries better than I did [here](https://www.bbc.com/future/article/20220511-can-gravity-batteries-solve-our-energy-storage-problems).
- [Energy Vault](https://www.energyvault.com/ldes) are another company working on gravity batteries.

<!-- setup mathjax -->
<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

[1]: https://www.bbc.com/future/article/20220511-can-gravity-batteries-solve-our-energy-storage-problems
[2]: https://en.wikipedia.org/wiki/Gravity_battery