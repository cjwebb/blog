+++
title = "Amazon Resource Names (ARNs)"
date = 2022-01-26

[taxonomies]
tags = ["aws", "devops", "wiki"]
+++

An Amazon Resource Name (ARN) uniquely identifies a thing in AWS. This is a quick recap on how they are structured.

<!-- more -->

ARNs have the following formats, with values separated by colons and forward-slashes.

```
arn:partition:service:region:account-id:resource-id
arn:partition:service:region:account-id:resource-type/resource-id
```

For example:

```
arn::s3:::my_bucket (s3 bucket, named my_bucket)
arn::iam::123456789012:user/JohnDoe (user JohnDoe, in account 123456789012)
```

## Omitting Values
You can omit parts of an ARN between colons, and AWS will infer the missing value for you.
For example, if you do not specify `account-id`, AWS infers that you mean 'the current account-id'. As such, put nothing between the colons where `account-id` goes:

```
arn:partition:service:region::resource-id
```

A usual part to omit from an ARN is `partition`.

Valid partition values are:
- aws (the default)
- aws-cn (AWS for   China)
- aws-us-gov (AWS for the US government)

Another example occurs when dealing with 'global' AWS services, such as S3.

S3 ARNs cannot have regions in them, as S3 doesn't expose regions to the user. This is why S3 Bucket Names are globally unique.

For example, some usual S3 ARNs, with values for region and account-id omitted:
```
arn:aws:s3:::minlist.co.uk
arn:aws:s3:::minlist.co.uk/index.html
```


## Resources
- [https://docs.aws.amazon.com/general/latest/gr/aws-arns-and-namespaces.html](https://docs.aws.amazon.com/general/latest/gr/aws-arns-and-namespaces.html)