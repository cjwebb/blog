+++
title = "About Me"
path = "/about"
+++


Hello, I'm Colin.

I'm a software engineer living in Oxfordshire, UK. You can also find me on
 <a href="https://twitter.com/colinjwebb">Twitter</a>,
 <a href="https://github.com/cjwebb">Github</a>, and
 <a href="https://www.linkedin.com/in/colinjwebb/">LinkedIn</a>.

On this site, I write about software, data-engineering, and other interesting technology.

I work as a consultant, covering small-startups, government departments, and multinationals - usually providing expertise and training on how to deliver projects faster, more iteratively, and with well-functioning teams.

I'm currently diving head-first into climate-related problems and projects, usually around using data to help transistion to a greener future. It's a cause close to my heart!

<!-- more -->

If you'd like to chat on that topic, or any others, please ping me social media (linked above).
