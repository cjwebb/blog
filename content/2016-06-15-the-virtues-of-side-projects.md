+++
title = "The Virtues of Side Projects"
date = 2016-06-15

[taxonomies]
tags = ["side-project"]
+++

I write software for a living, and most of my side projects are software based too. I view side projects as a tool for learning. Learning by doing.

<!-- more -->

## Side projects can feed positively into your day job, as you learn new skills and techniques

Building things from scratch, and by yourself, means you have to broaden your knowledge base. If you're writing a website, you'll need to know how to deploy it, how to enable database backups, how to configure a firewall, and basic server maintenance tools.

When I first started a side project, I wrote Java all day, and developed on Windows. I knew hardly anything about Linux, and certainly had no experience running a server. Whilst it wasn't necessarily all fun, I learnt a lot that has been widely applicable later on in my career.

Side projects can also assist in your day job directly. An example of this very happened recently to me. A couple of colleagues were pair programming on an adjacent desk. They were stuck on something, and casually asked if I knew how to fix a problem they were having. After a minute or so looking at the code, my answer was “Yeah, I did something like this recently in my side project”. I then sent them a link to my side project on Github, and after adapting my code, they had solved their issue and were away coding again.

Usually it's skills and techniques that will transfer over from side-projects. Occasionally, you can just copy some of your side project code.

## Side projects let you experiment without adversely affecting your day-job
Want to learn Haskell? Great! But don't then deploy it at work, and **force** other people to maintain your code.

Want to try out a theory about structuring an object-orientated codebase? You can, and if it doesn't work out, you won't have annoyed all of your colleagues.

Having a great job, or working on a great team, means that you're given time to learn or embrace different technologies or techniques. However, if you're not that lucky, side projects can give you the freedom to learn and experiment in ways you wouldn't normally be allowed to.

## Side projects can die without negative consequences
If you get bored with working on a side project, just stop working on it. A project with paying customers is not a side project. Nobody can honestly expect you to keep working for free on something you find boring.

Work on something different for a while. If the side project doesn't interest you again, forget about it, and thank it for the experience and skills you gained whilst working on it.

## Side projects can force you to think differently
Compared to your day job, side projects are subject to a different set of constraints. Different constraints means different problems to solve. Your decisions will be different as a result of this.

The main constraint on my side projects is money. At most places I've worked, the budget is usually huge in comparison to what I can set aside. EC2 instances costs nothing to a corporation, but ~$50/month, multiplied by three for redundancy and replication, soon make a dent in my own finances.

This constraint has also led me to use a lot of free-tiers of hosted-database providers. If a company is willing to provide me with free services, then I'm all ears. This was how I was introduced to NoSQL databases, and I gained a completely different perspective on how to store, and query, data.

## Side projects count as experience
Always be learning. There is a famous quote by Malcolm Gladwell about how doing something for ten thousand hours can help achieve mastery. Whilst this quote is often [out of context][quote], everyone understands that you need to practice something in order to become better at it. The good news, is that you can practice your skills whilst working on a fun side-project.

The second piece of good news, is that you can often use side projects as direct evidence to show off to potential employers. I've often chatted about my side projects whilst being interviewed, and I've often chatted about interviewees' side projects whilst I interviewed them. They're a great way to showcase your talents!

[quote]: https://www.reddit.com/r/IAmA/comments/2740ct/hi_im_malcolm_gladwell_author_of_the_tipping/chx6ku3
