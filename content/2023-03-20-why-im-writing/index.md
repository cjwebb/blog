+++
title = "Why I'm Writing"
date = 2023-03-20

[taxonomies]
tags = ["writing", "remote"]

[extra]
ogimage = "nick-morrison-FHnnjk1Yj7Y-unsplash.jpg"
+++

Why am I writing?

**To practice communication.**

Written analysis, documentation, and thought are powerful ways to communicate. Writing forces me to think, and to structure my thoughts. A clear structure enables clear communication.

<!-- more -->

I'm also writing **to build community**. I want to build interesting things, teach, and share what I've done. I also want to live in a world where others do the same.

Communication and community is essential **for remote work to thrive**. Remote work is the future. It is also the present, but I'm counting on it becoming more prevalent. Not least, for the reduction in environmental footprint of centralised offices and long commutes.

Why am I writing this?

So that I can read it when I don't feel like writing, and remind myself of why I should.


<br />

<img src='./nick-morrison-FHnnjk1Yj7Y-unsplash.jpg'/>

Photo by <a href="https://unsplash.com/@nickmorrison?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Nick Morrison</a> on <a href="https://unsplash.com/photos/FHnnjk1Yj7Y?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

(Now get some coffee and start writing)
