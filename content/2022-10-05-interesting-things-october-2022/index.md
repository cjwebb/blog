+++
title = "Interesting Things - October 2022"
date = 2022-10-05

[taxonomies]
tags = ["interesting", "metaverse"]
+++

In this edition of **Interesting Things** we're covering the technology of the Metaverse.

I've been reading Matthew Ball's [The Metaverse: And How It Will Revolutionize Everything](https://www.goodreads.com/book/show/59064518-the-metaverse).

Whether or not you believe the Metaverse is a thing that will exist, or should even exist, the technological challenges that need to be overcome are interesting.

<!-- more -->

<img src='header.png'/>

Section Two of the book is titled "Building the Metaverse" and is broken down into the following chapters.

- Networking
- Computing
- Virtual World Engines
- Interoperability
- Hardware
- Payment
- Blockchains

Each section goes through a brief history, highlights current problems, and considers potential future scope of the technology. There are many, many things that need to be addressed.

**Networking** is currently too slow. Both bandwidth and latency need to improve, even before you factor in the speed of light limiting cross-continental communication speeds. Zoom meetings today often have annoying connection issues. This problem will be exacerbated when we stream 3D models of ourselves, rather than a simple 2D video.

**Computing** still isn't powerful enough. It has increased exponentially year on year, but all the extra power always gets used up rapidly. We're still not at the level of hyper-realistic visuals, accurately modelled physics, or able to handle hundreds or thousands of players in the same space.

**Virtual World Engines** (such as the Unity and Unreal game engines) are getting there, but suffer because of slow networking and lack of compute to render detail.

Wearable **Hardware** is slow, and heavy, and battery-life is still a major constraint. Fitting photo-realistic displays into a lightweight pair of glasses is still years away.

There is almost no interoperability between virtual worlds, in terms of shared assets, shared decision-making, shared anything. One of the key tenets of the Metaverse is having an avatar. How does that get shared between various independent gaming institutions? **Payments** and **Blockchains** are likely a potential solution for facilitation, and non-centralised ownership and identity.

There are countless problems.

<br/>
<br/>

**The Interesting Thing** is that for every single problem listed above there are already countless ideas, opportunities, and exploratory projects tackling them!

Who would have thought:

- [Augmented-reality smart contact lenses already work](https://www.youtube.com/watch?v=cvgjVgmv5DM)
- [Haptic gloves](https://www.weart.it/touchdiver/) have launched already, so your fingers can touch things inside VR.
- AI techniques [DALL-E](https://openai.com/dall-e-2/), [Stable Diffusion](https://stability.ai/blog/stable-diffusion-announcement), and [NeRFs](https://www.matthewtancik.com/nerf) are posied to drastically increase artistic design cadence.
- [VR already shows benefits in training surgeons!](https://vrscout.com/news/vr-surgery-training-might-be-more-effective-than-we-thought/)
- [The Ethereum Merge finally happened](https://ethereum.org/en/upgrades/merge/) and reduced Ethereum's energy consumption by 99.9%, which is a big deal for sustainability!

This is just a small list of things. There is still huge potential to push boundaries, innovate, and drive what you want this future to look like.

<br/>
<br/>

If anything is interesting you at the moment, please let me know on Twitter - [@colinjwebb](https://twitter.com/colinjwebb)

For more Metaverse Interesting Things, I've started compiling [an XR Twitter List](https://twitter.com/i/lists/1568164798151462913) to keep track of news and cool projects in that domain.
