+++
title = "Rebooting AI - Further Reading"
date = 2019-10-18

[taxonomies]
tags = ["ai", "books"]
+++

I recently read [Rebooting AI](http://rebooting.ai/), as book discussing how the recent focus on Deep Learning as the path towards Artificial General Intelligence is flawed. It was a good read, and made many interesting points. It left me wanting to learn more about other areas of AI, and luckily there was a 'Suggested Reading' list at the end.

<!-- more -->

I decided to collate all the books, and articles in one place so I can refer to it easily afterwards. I've already ordered some of these books, and look forward to developing a more rounded knowledge base on these topics.

Unless otherwise stated, the links are for books on Amazon.co.uk

* __AI in General__
    * [Artificial Intelligence: A Modern Approach](https://www.amazon.co.uk/dp/1292153962) by Stuart Russell and Peter Norvig (2016)
    * [Future of Robotics and Artificial Intelligence](https://rodneybrooks.com/forai-future-of-robotics-and-artificial-intelligence) by Rodney Brooks (2017) - *Online Article*

* __Skepticism About AI__
    * [Computer Power and Human Reason](https://www.amazon.co.uk/dp/0716704633), by Joseph Weizenbaum (1977)
    * [What Computers Can’t Do](https://www.amazon.co.uk/dp/0060906138), by Hubert Dreyfus (1978)
    * [The AI Delusion](https://www.amazon.co.uk/dp/0198824300), by Gary Smith (2018)
    * [Artifictional Intelligence: Against Humanity’s Surrender to Computers](https://www.amazon.co.uk/dp/1509504125), by Harry Collins (2018)
    * [Artificial Unintelligence: How Computers Misunderstand the World](https://www.amazon.co.uk/dp/0262038005), by Meredith Broussard (2018)

* __What's at Stake__
    * [Weapons of Math Destruction](https://www.amazon.co.uk/dp/0553418815), by Cathy O’Neil (2016)
    * [Automating Inequality: How High-Tech Tools Profile, Police, and Punish the Poor](https://www.amazon.co.uk/dp/1250074312), by Virginia Eubanks (2018)

* __Machine Learning and Deep Learning__
    * [The Master Algorithm: How the Quest for the Ultimate Learning Machine Will Remake Our World](https://www.amazon.co.uk/dp/0241004543), by Pedro Domingos (2015)
    * [The Deep Learning Revolution](https://www.amazon.co.uk/dp/026203803X), by Terrence Sejnowski (2018)
    * [Machine Learning: A Probabilistic Perspective](https://www.amazon.co.uk/dp/0262018020), by Kevin Murphy (2012)
    * [Deep Learning](http://www.deeplearningbook.org/) by Ian Goodfellow, Yoshua Bengio, and Aaron Courville (2016) - *Online Book*

* __AI Systems That Read__
    * [Speech and Language Processing](https://web.stanford.edu/~jurafsky/slp3/ed3book.pdf), by Daniel Jurafsky and James H. Martin (2019) - *Online PDF*
    * [Foundations of Statistical Natural Language Processing](https://www.cs.vassar.edu/~cs366/docs/Manning_Schuetze_StatisticalNLP.pdf), by Christopher Manning and Hinrich Schütze (2000) - *Online PDF*
    * [Introduction to Information Retrieval](https://nlp.stanford.edu/IR-book/pdf/irbookonlinereading.pdf), by Christopher Manning, Prabhakar Raghavan, and Hinrich Schütze (2009) - *Online PDF*

* __Robotics__
    * [Toward Robotic Manipulation](https://www.annualreviews.org/doi/full/10.1146/annurev-control-060117-104848), by Matthew Mason (2018) - *Online Article*
    * [Modern Robotics: Mechanics, Planning, and Control](http://hades.mech.northwestern.edu/images/7/7f/MR.pdf), by Kevin Lynch and Frank Park (2017) - *Online PDF*
    * [Planning Algorithms](http://planning.cs.uiuc.edu/), by Steven LaValle (2006) - *Online Book*

* __Mind__
    * [The Language Instinct](https://www.amazon.co.uk/dp/0140175296), by Steven Pinker (1995)
    * [Words and Rules: The Ingredients of Language](https://www.amazon.co.uk/dp/0465072704), by Steven Pinker (2015)
    * [How the Mind Works and The Stuff of Thought](https://www.amazon.co.uk/dp/0141015470), by Steven Pinker (2008)
    * [Kluge](https://www.amazon.co.uk/dp/B002ECETZY), by Gary Marcus (2009)
    * [Thinking, Fast and Slow](https://www.amazon.co.uk/dp/0141033576), by Daniel Kahneman (2012)
    * [Brainstorms](https://www.amazon.co.uk/dp/0262040646/), by Daniel Dennett (1980)
    * [Human Knowledge: Its Scope and Limits](https://www.amazon.co.uk/dp/0415474442), by Bertrand Russell (2009)
    * [The Algebraic Mind](https://www.amazon.co.uk/dp/0262632683), by Gary Marcus (2003)

* __Commonsense Reasoning__
    * [Commonsense Reasoning and Commonsense Knowledge in Artificial
Intelligence](https://cs.nyu.edu/davise/papers/CommonsenseFinal.pdf), by Gary Marcus and Ernest Davis (2015) - *Online Paper*
    * [Common Sense, the Turing Test, and the Quest for Real AI](https://www.amazon.co.uk/dp/0262036045), by Hector Levesque (2017)
    * [Representations of Commonsense Knowledge](https://www.amazon.co.uk/dp/1483207706), by Ernest Davis (2014)
    * [The Handbook of Knowledge Representation](https://www.amazon.co.uk/dp/0444522115), by Frank van Harmelen, Vladimir Lifschitz, and Bruce Porter (2007)
    * [The Book of Why: The New Science of Cause and Effect](https://www.amazon.co.uk/dp/0241242630), by Judea Pearl and Dana Mackenzie (2018)

* __Trust__
    * [Moral Machines: Teaching Robots Right from Wrong](https://www.amazon.co.uk/dp/0199737975), by Wendell Wallach and Colin Allen (2010)
    * [Robot Ethics: The Ethical and Social Implications of Robotics](https://www.amazon.co.uk/dp/026252600X), by Patrick Lin, Keith Abney, and George Bekey (2014)
    * [SuperIntelligence: Paths, Dangers, Strategies](https://www.amazon.co.uk/dp/0199678111), by Nick Bostrom (2014)

* __Future of AI__
    * [Life 3.0: Being Human in the Age of Artificial Intelligence](https://www.amazon.co.uk/dp/024123719X), by Max Tegmark (2017)
    * [Abundance: The Future Is Better Than You Think](https://www.amazon.co.uk/dp/1451614217), by Peter Diamandis and Steven Kotler (2012)
    * [Our Final Invention: Artifical Intelligence and the End of the Human Era](https://www.amazon.co.uk/Our-Final-Invention-Artificial-Intelligence/dp/1250058783), by James Barrat (2015)

