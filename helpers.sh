#!/bin/bash
export BLOG_PATH="/Users/colin/kickstand/blog"

blog-new() {
    if [ -z "$1" ]
    then
        echo "No blog name supplied"
        return 1
    fi

    local today=$(date +%Y-%m-%d)
    local filename=$BLOG_PATH/content/$today-$1
    
    # error if file already exists
    if [ -f "$filename" ]
    then
        echo "A wiki post with this name already exists:"
        sed -e '1,5d' $filename | head
        return 1
    fi

    mkdir $filename
    cat<<EOT >> $filename/index.md
+++
title = "$1"
date = $(date +%F)
+++

EOT
}
